﻿
// game1View.cpp: реализация класса Cgame1View
//

#include "pch.h"
#include "framework.h"
// SHARED_HANDLERS можно определить в обработчиках фильтров просмотра реализации проекта ATL, эскизов
// и поиска; позволяет совместно использовать код документа в данным проекте.
#ifndef SHARED_HANDLERS
#include "game1.h"
#endif

#include "game1Doc.h"
#include "game1View.h"
#include "x_and_o.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Cgame1View

IMPLEMENT_DYNCREATE(Cgame1View, CView)

BEGIN_MESSAGE_MAP(Cgame1View, CView)
	// Стандартные команды печати
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &Cgame1View::OnFilePrintPreview)
	ON_WM_CONTEXTMENU()
	ON_WM_RBUTTONUP()
	ON_WM_CREATE()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()

// Создание или уничтожение Cgame1View

Cgame1View::Cgame1View() noexcept
{
	// TODO: добавьте код создания
	port = 8083;
	host = L"127.0.0.1";

	margin = 100;
	grids = 3;
	c_px_e = c_py_e = 0.;
	cross_cursor = LoadCursor(NULL, IDC_CROSS);
	arrow_cursor = LoadCursor(NULL, IDC_ARROW);

	control_grids = new game_struct[grids * grids];
	control_code = no_play;

	for (int i = 0; i < grids * grids; i++)
		control_grids[i].control = no_player;

	mem_dc = 0;
	bitmap = 0;

	font_georgia.lfHeight = 40;
	font_georgia.lfWidth = 0;
	font_georgia.lfEscapement = 0;
	font_georgia.lfOrientation = 0;
	font_georgia.lfWeight = FW_NORMAL;
	font_georgia.lfItalic = 0;
	font_georgia.lfUnderline = 0;
	font_georgia.lfStrikeOut = 0;
	font_georgia.lfCharSet = RUSSIAN_CHARSET;
	font_georgia.lfOutPrecision = OUT_TT_PRECIS;
	font_georgia.lfClipPrecision = CLIP_DEFAULT_PRECIS;
	font_georgia.lfQuality = PROOF_QUALITY;
	font_georgia.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	StrCpyW(font_georgia.lfFaceName, L"Georgia");

}

Cgame1View::~Cgame1View()
{
	if (mem_dc)
		delete mem_dc;
	if (bitmap)
		delete bitmap;
	if (control_grids)
		delete[] control_grids;

}

BOOL Cgame1View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: изменить класс Window или стили посредством изменения
	//  CREATESTRUCT cs

	cs.dwExStyle |= WS_EX_CLIENTEDGE;
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW |
		CS_VREDRAW | CS_DBLCLKS, ::LoadCursor(NULL,
			IDC_ARROW), HBRUSH(NULL), NULL);


	return CView::PreCreateWindow(cs);
}

int Cgame1View::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;

	//server.open(8082, GetSafeHwnd());
	// TODO:  Добавьте специализированный код создания

	mem_dc = new CDC;
	bitmap = new CBitmap;

	is_blue = true;

	return 0;
}


// Рисование Cgame1View

void Cgame1View::before_start(CDC* pDC) {
	GetClientRect(rect);

	mem_dc->DeleteDC();
	bitmap->DeleteObject();

	mem_dc->CreateCompatibleDC(pDC);
	bitmap->CreateCompatibleBitmap(pDC, rect.Width(), rect.Height());
	mem_dc->SelectObject(bitmap);

	mem_dc->Rectangle(rect);

	c_px_e = rect.Width() * 1. / grids;
	c_py_e = ((long long)rect.Height() - margin) * 1. / grids;
}

void Cgame1View::OnDraw(CDC* pDC)
{
	Cgame1Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	before_start(pDC);

	if (!control_code) {
		x_and_o game;
		
		while (true) {

			if (game.DoModal() == IDOK) {
				if (server.open(port, this)) {
					control_code = wait_server;
					GetParentFrame()->SetWindowTextW(L"Wait player...");
					break;
				}
				else { AfxMessageBox(L"Can\'t be host on this port"); }
			}
			else {
				if (!client.connect(host, port, this)) {
					AfxMessageBox(L"Can\'t connect to host");
					continue;
				}
				break;
			}

		}

	}

	else if (control_code == wait_server) {
		GetParentFrame()->SetWindowTextW(L"Wait player...");
	}
	else if (control_code == start_game) {
		setting_grid();
		show_grid();
		show_elements();

		if (client.get_socket() != INVALID_SOCKET)
			GetParentFrame()->SetWindowTextW(L"You: Red");
		else
			GetParentFrame()->SetWindowTextW(L"You: Blue");
	}

	pDC->BitBlt(0, 0, rect.Width(), rect.Height(), mem_dc, 0, 0, SRCCOPY);

	// TODO: добавьте здесь код отрисовки для собственных данных
}

void Cgame1View::setting_grid() {

	int control_height = 0;
	int control_width = 0;
	for (int i = 0; i < grids * grids; i++, control_width++) {
		if (i && (i % grids) == 0) {
			control_height++;
			control_width = 0;
		}
		control_grids[i].rect.left = control_width * c_px_e;
		control_grids[i].rect.right = (control_width + 1) * c_px_e;
		control_grids[i].rect.top = margin + control_height * c_py_e;
		control_grids[i].rect.bottom = margin + (control_height + 1) * c_py_e;
	}
}

void Cgame1View::show_grid() {

	CPen pen, pen2, *pen_st;
	pen.CreatePen(1, PS_DASH, RGB(15, 15, 15));
	pen_st = mem_dc->SelectObject(&pen);
	
	for (int i = 1; i < grids; i++) {
		mem_dc->MoveTo(0, margin + i * c_py_e);
		mem_dc->LineTo(grids * c_px_e, margin + i * c_py_e);

		mem_dc->MoveTo(i * c_px_e, margin);
		mem_dc->LineTo(i * c_px_e, margin + grids * c_py_e);
	}
	
	COLORREF color, old_color;
	CFont font;
	CString step;
	int old_mode;

	if (is_blue) {
		color = RGB(25, 25, 225);
		pen2.CreatePen(1, PS_DOT, RGB(25, 25, 225));
		step += L"blue move";
	}
	else {
		color = RGB(225, 25, 25);
		pen2.CreatePen(1, PS_DOT, RGB(225, 25, 25));
		step += L"red move";
	}

	font.CreateFontIndirectW(&font_georgia);

	old_color = mem_dc->SetTextColor(color);
	old_mode = mem_dc->SetBkMode(TRANSPARENT);
	mem_dc->SelectObject(&font);

	CSize size = mem_dc->GetTextExtent(step);

	mem_dc->SelectObject(&pen2);
	mem_dc->MoveTo(0, margin);
	mem_dc->LineTo(c_px_e * c_px_e, margin);
	mem_dc->TextOutW(rect.Width() / 2 - size.cx / 2 , (int)(margin * 0.2), step);

	mem_dc->SelectObject(pen_st);
	mem_dc->SetTextColor(old_color);
	mem_dc->SetBkMode(old_mode);

}

void Cgame1View::show_elements() {
	
	CPen pen, * pen_st;
	
	pen_st = mem_dc->SelectObject(&pen);
	
	for (int i = 0; i < grids * grids; i++) {
		
		CRect inflate_rect = control_grids[i].rect;
		inflate_rect.InflateRect(-inflate_rect.Width() * 0.02, -inflate_rect.Height() * 0.02);

		pen.DeleteObject();

		if (control_grids[i].control == cross)
			pen.CreatePen(PS_SOLID, 5, RGB(25, 25, 225));

		else if(control_grids[i].control == zero)
			pen.CreatePen(PS_SOLID, 5, RGB(225, 25, 25));
		
		mem_dc->SelectObject(&pen);

		if (control_grids[i].control == cross) {
			mem_dc->MoveTo(inflate_rect.left, inflate_rect.top);
			mem_dc->LineTo(inflate_rect.right, inflate_rect.bottom);
			mem_dc->MoveTo(inflate_rect.right, inflate_rect.top);
			mem_dc->LineTo(inflate_rect.left, inflate_rect.bottom);
		}

		else if(control_grids[i].control == zero)
			mem_dc->Ellipse(inflate_rect);

	}


	mem_dc->SelectObject(pen_st);

}

int Cgame1View::is_win() {

	for (int r = 0; r < 2; r++) {

		int choice = (r) ? zero : cross;

		int i, j, z, count_choice;

		for (i = 0; i < (grids * grids) - grids; i++) {
			count_choice = 0;
			for (j = 0, z = 0; j < grids; j++, z += grids) {
				if ((i < grids) && control_grids[(grids * i) + j].control == choice)
					count_choice++;
				else if ((i >= grids) && control_grids[(i - grids) + z].control == choice)
					count_choice++;
				if (count_choice == grids) return choice;
			}
		}

		count_choice = 0;
		for (i = 0, j = 0; i < grids * 2; i++) {
			if (i == grids) {
				count_choice = 0;
				j -= grids * 2;
			}


			if (i < grids) {
				if (control_grids[j].control == choice)
					count_choice++;

				j += grids + 1;
			}


			else if (i >= grids) {
				if (control_grids[j].control == choice)
					count_choice++;

				j -= grids - 1;
			}

			if (count_choice == grids) return choice;
		}

	}
	return no_player;
}

// Печать Cgame1View


void Cgame1View::OnFilePrintPreview()
{
#ifndef SHARED_HANDLERS
	AFXPrintPreview(this);
#endif
}

BOOL Cgame1View::OnPreparePrinting(CPrintInfo* pInfo)
{
	// подготовка по умолчанию
	return DoPreparePrinting(pInfo);
}

void Cgame1View::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: добавьте дополнительную инициализацию перед печатью
}

void Cgame1View::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: добавьте очистку после печати
}

void Cgame1View::OnRButtonUp(UINT /* nFlags */, CPoint point)
{
	ClientToScreen(&point);
	OnContextMenu(this, point);
}

void Cgame1View::OnContextMenu(CWnd* /* pWnd */, CPoint point)
{
#ifndef SHARED_HANDLERS
	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
#endif
}


// Диагностика Cgame1View

#ifdef _DEBUG
void Cgame1View::AssertValid() const
{
	CView::AssertValid();
}

void Cgame1View::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

Cgame1Doc* Cgame1View::GetDocument() const // встроена неотлаженная версия
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(Cgame1Doc)));
	return (Cgame1Doc*)m_pDocument;
}
#endif //_DEBUG


// Обработчики сообщений Cgame1View

void Cgame1View::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: добавьте свой код обработчика сообщений или вызов стандартного

	if (control_code == start_game) {

		CRect game_field = rect;
		game_field.top += margin;

		if (game_field.PtInRect(point)) {
			if (GetCursor() != cross_cursor)
				SetCursor(cross_cursor);
			
			CView::OnMouseMove(nFlags, point);
			return;
		}
		SetCursor(arrow_cursor);
	}

	CView::OnMouseMove(nFlags, point);
}

void Cgame1View::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: добавьте свой код обработчика сообщений или вызов стандартного

	if (is_blue && client.get_socket() != INVALID_SOCKET) return CView::OnLButtonUp(nFlags, point);
	else if (!is_blue && server.get_socket() != INVALID_SOCKET) return CView::OnLButtonUp(nFlags, point);

	if (control_code == start_game && !is_win()) {

		game_struct step;

		for (int i = 0; i < grids * grids; i++) {
			if (control_grids[i].rect.PtInRect(point)) {
				if (control_grids[i].control == no_player) {
					if (client.get_socket() != INVALID_SOCKET) {
						control_grids[i].control = zero;
						is_blue = true;
						socket_data data(i, zero, is_blue);
						client.Send((void*)&data, sizeof(socket_data));
					}
					else {
						control_grids[i].control = cross;
						is_blue = false;
						socket_data data(i, cross, is_blue);
						server.send_to_client(data);
					}
					Invalidate();
					if (is_win() == cross)
						AfxMessageBox(L"Blue win");
					else if(is_win() == zero)
						AfxMessageBox(L"Red win");
					return CView::OnLButtonUp(nFlags, point);
				}
				else { return  CView::OnLButtonUp(nFlags, point); }
			}
		}

	}

	CView::OnLButtonUp(nFlags, point);

}