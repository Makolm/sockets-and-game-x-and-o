﻿
// game1View.h: интерфейс класса Cgame1View
//

#pragma once

#include "socket_standart.h"
#include "game1Doc.h"

struct game_struct {
	CRect rect;
	int control;
};

class Cgame1View : public CView
{
protected: // создать только из сериализации
	Cgame1View() noexcept;
	DECLARE_DYNCREATE(Cgame1View)

// Атрибуты
public:
	Cgame1Doc* GetDocument() const;
	server_socket server;
	client_socket client;

	int port;
	CString host;
	
	CRect rect;
	int grids;
	int margin;
	double c_px_e, c_py_e;

	int control_code;
	enum controls{
		no_play,
		wait_server,
		start_game,
	};

	CDC* mem_dc;
	CBitmap* bitmap;
	LOGFONT font_georgia;
	HCURSOR cross_cursor, arrow_cursor;

	game_struct* control_grids;
	bool is_blue;

	enum grid_control {
		no_player,
		zero,
		cross
	};

// Операции
public:
	void before_start(CDC* pDC);
	void setting_grid();
	void show_grid();
	void show_elements();
	int is_win();
// Переопределение
public:
	virtual void OnDraw(CDC* pDC);  // переопределено для отрисовки этого представления
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// Реализация
public:
	virtual ~Cgame1View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Созданные функции схемы сообщений
protected:
	afx_msg void OnFilePrintPreview();
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
};

#ifndef _DEBUG  // версия отладки в game1View.cpp
inline Cgame1Doc* Cgame1View::GetDocument() const
   { return reinterpret_cast<Cgame1Doc*>(m_pDocument); }
#endif