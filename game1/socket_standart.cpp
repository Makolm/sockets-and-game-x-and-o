#include "pch.h"
#include "socket_standart.h"
#include "game1View.h"



socket_data::socket_data(int g, int c, bool i) {
	grid = g;
	control = c;
	is_blue = i;
}

standart_socket::standart_socket() {
	for_game_first = 0;
	view = 0;
}

void standart_socket::set_first_status_game(bool c) { for_game_first = c; }

bool client_socket::connect(CString host, int port, CView* view) {
	Close();
	if (!Socket()) {
		m_hSocket = INVALID_SOCKET;
	}
	else if (!Connect(host, port)) {
		m_hSocket = INVALID_SOCKET;
	} 
	if (m_hSocket == INVALID_SOCKET) {
		Close();
		return false;
	}
	this->view = view;
	return true;
}

bool server_socket::open(int port, CView* view) {
	Close();
	if (!Create(port)) {
		m_hSocket = INVALID_SOCKET;
	}
	else if (!Listen()) {
		m_hSocket = INVALID_SOCKET;
	}
	if (m_hSocket == INVALID_SOCKET) {
		Close();
		return false;
	}
	this->view = view;
	return true;
}

void server_socket::send_to_client(socket_data& data) {
	client.Send((void*)&data, sizeof(socket_data));
}

void server_socket::OnAccept(int error) {
	if (Accept(client)) {
		srand(time(NULL));
		Cgame1View* c_view = ((Cgame1View*)view);
		c_view->is_blue = rand() % 3;
		client.Send(&(c_view->is_blue), sizeof(bool));
		client.set_first_status_game(true);
		client.set_view(view);
		c_view->control_code = Cgame1View::start_game;
		c_view->Invalidate();
	}
	
}

void client_socket::OnReceive(int errMsg) {

	Cgame1View* c_view = ((Cgame1View*)view);

	if (!for_game_first) {
		bool c;
		Receive(&c, sizeof(bool));
		c_view->is_blue = c;
		set_first_status_game(true);
		c_view->control_code = Cgame1View::start_game;
		c_view->Invalidate();
	}
	else {
		socket_data data;
		Receive((void*)&data, sizeof(socket_data));
	
		c_view->control_grids[data.grid].control = data.control;
		c_view->is_blue = data.is_blue;
		c_view->Invalidate();
	}

	if (c_view->is_win() == Cgame1View::cross)
		AfxMessageBox(L"Blue win");
	else if (c_view->is_win() == Cgame1View::zero)
		AfxMessageBox(L"Red win");

}