#pragma once


#define WM_SOCKETEVENT WM_USER + 1


struct socket_data {
	int grid;
	int control;
	bool is_blue;

	socket_data() {};
	socket_data(int, int, bool);
};


class standart_socket : public CSocket {
protected:
	CView* view;
	bool for_game_first;
public:

	standart_socket();
	void set_first_status_game(bool);
	SOCKET get_socket(){ return m_hSocket; };

};

class client_socket : public standart_socket {

public:

	void set_view(CView* view) { this->view = view; }
	bool connect(CString, int, CView*);
	void OnReceive(int);

};

class server_socket : public standart_socket {
	client_socket client;
public:

	bool open(int port, CView* view);
	void send_to_client(socket_data&);
	void OnAccept(int);

};