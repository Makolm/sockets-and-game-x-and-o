﻿// x_and_o.cpp: файл реализации
//

#include "pch.h"
#include "game1.h"
#include "x_and_o.h"
#include "afxdialogex.h"


// Диалоговое окно x_and_o

IMPLEMENT_DYNAMIC(x_and_o, CDialogEx)

x_and_o::x_and_o(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DIALOG1, pParent)
{

}

x_and_o::~x_and_o()
{
}

void x_and_o::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(x_and_o, CDialogEx)
	ON_BN_CLICKED(IDC_HOST, &x_and_o::OnBnClickedHost)
	ON_BN_CLICKED(IDC_CONNECT, &x_and_o::OnBnClickedConnect)
END_MESSAGE_MAP()


// Обработчики сообщений x_and_o


void x_and_o::OnBnClickedHost()
{
	// TODO: добавьте свой код обработчика уведомлений
	OnOK();
}


void x_and_o::OnBnClickedConnect()
{
	// TODO: добавьте свой код обработчика уведомлений
	OnCancel();
}
