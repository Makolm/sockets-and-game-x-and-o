﻿#pragma once


// Диалоговое окно x_and_o

class x_and_o : public CDialogEx
{
	DECLARE_DYNAMIC(x_and_o)

public:
	x_and_o(CWnd* pParent = nullptr);   // стандартный конструктор
	virtual ~x_and_o();

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG1 };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // поддержка DDX/DDV

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedHost();
	afx_msg void OnBnClickedConnect();
};
